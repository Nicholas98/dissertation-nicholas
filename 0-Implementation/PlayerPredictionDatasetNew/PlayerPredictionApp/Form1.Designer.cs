﻿namespace PlayerPredictionApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblScores = new System.Windows.Forms.Label();
            this.lblDataset = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPlayerStats = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblChooseAlgorithm = new System.Windows.Forms.Label();
            this.comboBoxAlgorithm = new System.Windows.Forms.ComboBox();
            this.lblPlayerPosition = new System.Windows.Forms.Label();
            this.btnGeneratePosition = new System.Windows.Forms.Button();
            this.txtChoosePlayer = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lblGoalkeeping = new System.Windows.Forms.Label();
            this.lblReflexes = new System.Windows.Forms.Label();
            this.lblHandling = new System.Windows.Forms.Label();
            this.lblDiving = new System.Windows.Forms.Label();
            this.lblPhysical = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDribblingText = new System.Windows.Forms.Label();
            this.lblPassing = new System.Windows.Forms.Label();
            this.lblShooting = new System.Windows.Forms.Label();
            this.lblPace = new System.Windows.Forms.Label();
            this.lblAggression = new System.Windows.Forms.Label();
            this.lblSlidingTackle = new System.Windows.Forms.Label();
            this.txtSlidingTackle = new System.Windows.Forms.TextBox();
            this.txtAggression = new System.Windows.Forms.TextBox();
            this.lblStamina = new System.Windows.Forms.Label();
            this.lblLongShots = new System.Windows.Forms.Label();
            this.txtLongShots = new System.Windows.Forms.TextBox();
            this.txtStamina = new System.Windows.Forms.TextBox();
            this.lblReactions = new System.Windows.Forms.Label();
            this.lblBalance = new System.Windows.Forms.Label();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.txtReactions = new System.Windows.Forms.TextBox();
            this.GK3 = new System.Windows.Forms.TextBox();
            this.GK2 = new System.Windows.Forms.TextBox();
            this.GK1 = new System.Windows.Forms.TextBox();
            this.txtPreferredFoot = new System.Windows.Forms.TextBox();
            this.lblPreferredFoot = new System.Windows.Forms.Label();
            this.lblShotPower = new System.Windows.Forms.Label();
            this.lblMarking = new System.Windows.Forms.Label();
            this.txtMarking = new System.Windows.Forms.TextBox();
            this.txtShotPower = new System.Windows.Forms.TextBox();
            this.lblBallControl = new System.Windows.Forms.Label();
            this.lblCurve = new System.Windows.Forms.Label();
            this.txtCurve = new System.Windows.Forms.TextBox();
            this.txtBallControl = new System.Windows.Forms.TextBox();
            this.lblLongPassing = new System.Windows.Forms.Label();
            this.lblVision = new System.Windows.Forms.Label();
            this.txtVision = new System.Windows.Forms.TextBox();
            this.txtLongPassing = new System.Windows.Forms.TextBox();
            this.lblInterceptions = new System.Windows.Forms.Label();
            this.lblCrossing = new System.Windows.Forms.Label();
            this.txtCrossing = new System.Windows.Forms.TextBox();
            this.txtInterceptions = new System.Windows.Forms.TextBox();
            this.lblDribbling = new System.Windows.Forms.Label();
            this.lblTackle = new System.Windows.Forms.Label();
            this.txtDribbling = new System.Windows.Forms.TextBox();
            this.txtStandingTackle = new System.Windows.Forms.TextBox();
            this.lblVolleys = new System.Windows.Forms.Label();
            this.lblFinishing = new System.Windows.Forms.Label();
            this.txtVolleys = new System.Windows.Forms.TextBox();
            this.txtFinishing = new System.Windows.Forms.TextBox();
            this.lblStrength = new System.Windows.Forms.Label();
            this.lblAcceleration = new System.Windows.Forms.Label();
            this.lblJumping = new System.Windows.Forms.Label();
            this.lblShortPassing = new System.Windows.Forms.Label();
            this.lblAgility = new System.Windows.Forms.Label();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.txtStrength = new System.Windows.Forms.TextBox();
            this.txtJumping = new System.Windows.Forms.TextBox();
            this.txtAgility = new System.Windows.Forms.TextBox();
            this.txtShortPassing = new System.Windows.Forms.TextBox();
            this.txtAcceleration = new System.Windows.Forms.TextBox();
            this.txtSpeed = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.createAFormationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertPlayerStatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playerPredictionSurveyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playersDataSet = new PlayerPredictionApp.PlayersDataSet();
            this.playersPredictingDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.playersPredictingDataTableAdapter = new PlayerPredictionApp.PlayersDataSetTableAdapters.playersPredictingDataTableAdapter();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playersDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playersPredictingDataBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lblScores
            // 
            this.lblScores.Location = new System.Drawing.Point(12, 401);
            this.lblScores.Name = "lblScores";
            this.lblScores.Size = new System.Drawing.Size(323, 50);
            this.lblScores.TabIndex = 274;
            // 
            // lblDataset
            // 
            this.lblDataset.AutoSize = true;
            this.lblDataset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblDataset.Font = new System.Drawing.Font("Car Lock", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataset.Location = new System.Drawing.Point(284, 9);
            this.lblDataset.Name = "lblDataset";
            this.lblDataset.Size = new System.Drawing.Size(360, 29);
            this.lblDataset.TabIndex = 272;
            this.lblDataset.Text = "PLAYER POSITION PREDICTION ";
            this.lblDataset.Click += new System.EventHandler(this.lblDataset_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel1.Controls.Add(this.lblDataset);
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(906, 47);
            this.panel1.TabIndex = 273;
            // 
            // lblPlayerStats
            // 
            this.lblPlayerStats.AutoSize = true;
            this.lblPlayerStats.Font = new System.Drawing.Font("Car Lock", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerStats.Location = new System.Drawing.Point(378, 86);
            this.lblPlayerStats.Name = "lblPlayerStats";
            this.lblPlayerStats.Size = new System.Drawing.Size(147, 22);
            this.lblPlayerStats.TabIndex = 271;
            this.lblPlayerStats.Text = "PLAYER STATS";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(341, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(2, 450);
            this.label2.TabIndex = 270;
            // 
            // lblChooseAlgorithm
            // 
            this.lblChooseAlgorithm.AutoSize = true;
            this.lblChooseAlgorithm.Font = new System.Drawing.Font("Car Lock", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChooseAlgorithm.Location = new System.Drawing.Point(79, 160);
            this.lblChooseAlgorithm.Name = "lblChooseAlgorithm";
            this.lblChooseAlgorithm.Size = new System.Drawing.Size(156, 16);
            this.lblChooseAlgorithm.TabIndex = 269;
            this.lblChooseAlgorithm.Text = "Choose Algorithm";
            // 
            // comboBoxAlgorithm
            // 
            this.comboBoxAlgorithm.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxAlgorithm.FormattingEnabled = true;
            this.comboBoxAlgorithm.Items.AddRange(new object[] {
            "Fast Tree",
            "Linear SVM"});
            this.comboBoxAlgorithm.Location = new System.Drawing.Point(83, 188);
            this.comboBoxAlgorithm.Name = "comboBoxAlgorithm";
            this.comboBoxAlgorithm.Size = new System.Drawing.Size(144, 25);
            this.comboBoxAlgorithm.TabIndex = 268;
            // 
            // lblPlayerPosition
            // 
            this.lblPlayerPosition.Font = new System.Drawing.Font("Car Lock", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerPosition.Location = new System.Drawing.Point(58, 298);
            this.lblPlayerPosition.Name = "lblPlayerPosition";
            this.lblPlayerPosition.Size = new System.Drawing.Size(195, 65);
            this.lblPlayerPosition.TabIndex = 267;
            this.lblPlayerPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnGeneratePosition
            // 
            this.btnGeneratePosition.Font = new System.Drawing.Font("Car Lock", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGeneratePosition.Location = new System.Drawing.Point(53, 465);
            this.btnGeneratePosition.Name = "btnGeneratePosition";
            this.btnGeneratePosition.Size = new System.Drawing.Size(208, 51);
            this.btnGeneratePosition.TabIndex = 266;
            this.btnGeneratePosition.Text = "Generate Position";
            this.btnGeneratePosition.UseVisualStyleBackColor = true;
            this.btnGeneratePosition.Click += new System.EventHandler(this.btnGeneratePosition_Click);
            // 
            // txtChoosePlayer
            // 
            this.txtChoosePlayer.AutoSize = true;
            this.txtChoosePlayer.Font = new System.Drawing.Font("Car Lock", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChoosePlayer.Location = new System.Drawing.Point(78, 86);
            this.txtChoosePlayer.Name = "txtChoosePlayer";
            this.txtChoosePlayer.Size = new System.Drawing.Size(165, 22);
            this.txtChoosePlayer.TabIndex = 265;
            this.txtChoosePlayer.Text = "Choose Player";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.playersPredictingDataBindingSource;
            this.comboBox1.DisplayMember = "name";
            this.comboBox1.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(51, 122);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(209, 25);
            this.comboBox1.TabIndex = 264;
            // 
            // lblGoalkeeping
            // 
            this.lblGoalkeeping.AutoSize = true;
            this.lblGoalkeeping.Font = new System.Drawing.Font("Car Lock", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoalkeeping.Location = new System.Drawing.Point(771, 434);
            this.lblGoalkeeping.Name = "lblGoalkeeping";
            this.lblGoalkeeping.Size = new System.Drawing.Size(111, 16);
            this.lblGoalkeeping.TabIndex = 263;
            this.lblGoalkeeping.Text = "GOALKEEPING";
            this.lblGoalkeeping.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblReflexes
            // 
            this.lblReflexes.AutoSize = true;
            this.lblReflexes.Location = new System.Drawing.Point(792, 465);
            this.lblReflexes.Name = "lblReflexes";
            this.lblReflexes.Size = new System.Drawing.Size(48, 13);
            this.lblReflexes.TabIndex = 262;
            this.lblReflexes.Text = "Reflexes";
            // 
            // lblHandling
            // 
            this.lblHandling.AutoSize = true;
            this.lblHandling.Location = new System.Drawing.Point(684, 465);
            this.lblHandling.Name = "lblHandling";
            this.lblHandling.Size = new System.Drawing.Size(49, 13);
            this.lblHandling.TabIndex = 261;
            this.lblHandling.Text = "Handling";
            // 
            // lblDiving
            // 
            this.lblDiving.AutoSize = true;
            this.lblDiving.Location = new System.Drawing.Point(579, 466);
            this.lblDiving.Name = "lblDiving";
            this.lblDiving.Size = new System.Drawing.Size(37, 13);
            this.lblDiving.TabIndex = 260;
            this.lblDiving.Text = "Diving";
            // 
            // lblPhysical
            // 
            this.lblPhysical.AutoSize = true;
            this.lblPhysical.Font = new System.Drawing.Font("Car Lock", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhysical.Location = new System.Drawing.Point(802, 377);
            this.lblPhysical.Name = "lblPhysical";
            this.lblPhysical.Size = new System.Drawing.Size(81, 16);
            this.lblPhysical.TabIndex = 259;
            this.lblPhysical.Text = "PHYSICAL";
            this.lblPhysical.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Car Lock", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(789, 320);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 16);
            this.label1.TabIndex = 258;
            this.label1.Text = "DEFENDING";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDribblingText
            // 
            this.lblDribblingText.AutoSize = true;
            this.lblDribblingText.Font = new System.Drawing.Font("Car Lock", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDribblingText.Location = new System.Drawing.Point(796, 262);
            this.lblDribblingText.Name = "lblDribblingText";
            this.lblDribblingText.Size = new System.Drawing.Size(85, 16);
            this.lblDribblingText.TabIndex = 257;
            this.lblDribblingText.Text = "DRIBBLING";
            this.lblDribblingText.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblPassing
            // 
            this.lblPassing.AutoSize = true;
            this.lblPassing.Font = new System.Drawing.Font("Car Lock", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassing.Location = new System.Drawing.Point(809, 200);
            this.lblPassing.Name = "lblPassing";
            this.lblPassing.Size = new System.Drawing.Size(71, 16);
            this.lblPassing.TabIndex = 256;
            this.lblPassing.Text = "PASSING";
            // 
            // lblShooting
            // 
            this.lblShooting.AutoSize = true;
            this.lblShooting.Font = new System.Drawing.Font("Car Lock", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShooting.Location = new System.Drawing.Point(799, 140);
            this.lblShooting.Name = "lblShooting";
            this.lblShooting.Size = new System.Drawing.Size(81, 16);
            this.lblShooting.TabIndex = 255;
            this.lblShooting.Text = "SHOOTING";
            // 
            // lblPace
            // 
            this.lblPace.AutoSize = true;
            this.lblPace.Font = new System.Drawing.Font("Car Lock", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPace.Location = new System.Drawing.Point(833, 85);
            this.lblPace.Name = "lblPace";
            this.lblPace.Size = new System.Drawing.Size(47, 16);
            this.lblPace.TabIndex = 254;
            this.lblPace.Text = "PACE";
            // 
            // lblAggression
            // 
            this.lblAggression.AutoSize = true;
            this.lblAggression.Location = new System.Drawing.Point(488, 406);
            this.lblAggression.Name = "lblAggression";
            this.lblAggression.Size = new System.Drawing.Size(59, 13);
            this.lblAggression.TabIndex = 253;
            this.lblAggression.Text = "Aggression";
            // 
            // lblSlidingTackle
            // 
            this.lblSlidingTackle.AutoSize = true;
            this.lblSlidingTackle.Location = new System.Drawing.Point(633, 350);
            this.lblSlidingTackle.Name = "lblSlidingTackle";
            this.lblSlidingTackle.Size = new System.Drawing.Size(74, 13);
            this.lblSlidingTackle.TabIndex = 252;
            this.lblSlidingTackle.Text = "Sliding Tackle";
            // 
            // txtSlidingTackle
            // 
            this.txtSlidingTackle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "sliding_tackle", true));
            this.txtSlidingTackle.Location = new System.Drawing.Point(713, 347);
            this.txtSlidingTackle.Name = "txtSlidingTackle";
            this.txtSlidingTackle.Size = new System.Drawing.Size(30, 20);
            this.txtSlidingTackle.TabIndex = 251;
            // 
            // txtAggression
            // 
            this.txtAggression.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "aggression", true));
            this.txtAggression.Location = new System.Drawing.Point(552, 403);
            this.txtAggression.Name = "txtAggression";
            this.txtAggression.Size = new System.Drawing.Size(30, 20);
            this.txtAggression.TabIndex = 250;
            // 
            // lblStamina
            // 
            this.lblStamina.AutoSize = true;
            this.lblStamina.Location = new System.Drawing.Point(599, 406);
            this.lblStamina.Name = "lblStamina";
            this.lblStamina.Size = new System.Drawing.Size(45, 13);
            this.lblStamina.TabIndex = 249;
            this.lblStamina.Text = "Stamina";
            // 
            // lblLongShots
            // 
            this.lblLongShots.AutoSize = true;
            this.lblLongShots.Location = new System.Drawing.Point(690, 170);
            this.lblLongShots.Name = "lblLongShots";
            this.lblLongShots.Size = new System.Drawing.Size(61, 13);
            this.lblLongShots.TabIndex = 248;
            this.lblLongShots.Text = "Long Shots";
            // 
            // txtLongShots
            // 
            this.txtLongShots.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "long_shots", true));
            this.txtLongShots.Location = new System.Drawing.Point(754, 167);
            this.txtLongShots.Name = "txtLongShots";
            this.txtLongShots.Size = new System.Drawing.Size(30, 20);
            this.txtLongShots.TabIndex = 247;
            // 
            // txtStamina
            // 
            this.txtStamina.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "stamina", true));
            this.txtStamina.Location = new System.Drawing.Point(650, 403);
            this.txtStamina.Name = "txtStamina";
            this.txtStamina.Size = new System.Drawing.Size(30, 20);
            this.txtStamina.TabIndex = 246;
            // 
            // lblReactions
            // 
            this.lblReactions.AutoSize = true;
            this.lblReactions.Location = new System.Drawing.Point(783, 291);
            this.lblReactions.Name = "lblReactions";
            this.lblReactions.Size = new System.Drawing.Size(55, 13);
            this.lblReactions.TabIndex = 245;
            this.lblReactions.Text = "Reactions";
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Location = new System.Drawing.Point(679, 291);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(46, 13);
            this.lblBalance.TabIndex = 244;
            this.lblBalance.Text = "Balance";
            // 
            // txtBalance
            // 
            this.txtBalance.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "balance", true));
            this.txtBalance.Location = new System.Drawing.Point(731, 288);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(30, 20);
            this.txtBalance.TabIndex = 243;
            // 
            // txtReactions
            // 
            this.txtReactions.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "reactions", true));
            this.txtReactions.Location = new System.Drawing.Point(847, 288);
            this.txtReactions.Name = "txtReactions";
            this.txtReactions.Size = new System.Drawing.Size(30, 20);
            this.txtReactions.TabIndex = 242;
            // 
            // GK3
            // 
            this.GK3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "goalkeeping_reflexes", true));
            this.GK3.Location = new System.Drawing.Point(847, 461);
            this.GK3.Name = "GK3";
            this.GK3.Size = new System.Drawing.Size(30, 20);
            this.GK3.TabIndex = 240;
            // 
            // GK2
            // 
            this.GK2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "goalkeeping_handling", true));
            this.GK2.Location = new System.Drawing.Point(736, 462);
            this.GK2.Name = "GK2";
            this.GK2.Size = new System.Drawing.Size(30, 20);
            this.GK2.TabIndex = 239;
            // 
            // GK1
            // 
            this.GK1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "goalkeeping_diving", true));
            this.GK1.Location = new System.Drawing.Point(628, 462);
            this.GK1.Name = "GK1";
            this.GK1.Size = new System.Drawing.Size(30, 20);
            this.GK1.TabIndex = 238;
            // 
            // txtPreferredFoot
            // 
            this.txtPreferredFoot.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "preferred_foot", true));
            this.txtPreferredFoot.Location = new System.Drawing.Point(812, 523);
            this.txtPreferredFoot.Name = "txtPreferredFoot";
            this.txtPreferredFoot.Size = new System.Drawing.Size(64, 20);
            this.txtPreferredFoot.TabIndex = 237;
            // 
            // lblPreferredFoot
            // 
            this.lblPreferredFoot.AutoSize = true;
            this.lblPreferredFoot.Font = new System.Drawing.Font("Car Lock", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreferredFoot.Location = new System.Drawing.Point(742, 495);
            this.lblPreferredFoot.Name = "lblPreferredFoot";
            this.lblPreferredFoot.Size = new System.Drawing.Size(141, 16);
            this.lblPreferredFoot.TabIndex = 236;
            this.lblPreferredFoot.Text = "PREFERRED FOOT";
            // 
            // lblShotPower
            // 
            this.lblShotPower.AutoSize = true;
            this.lblShotPower.Location = new System.Drawing.Point(577, 171);
            this.lblShotPower.Name = "lblShotPower";
            this.lblShotPower.Size = new System.Drawing.Size(62, 13);
            this.lblShotPower.TabIndex = 235;
            this.lblShotPower.Text = "Shot Power";
            // 
            // lblMarking
            // 
            this.lblMarking.AutoSize = true;
            this.lblMarking.Location = new System.Drawing.Point(539, 351);
            this.lblMarking.Name = "lblMarking";
            this.lblMarking.Size = new System.Drawing.Size(45, 13);
            this.lblMarking.TabIndex = 234;
            this.lblMarking.Text = "Marking";
            // 
            // txtMarking
            // 
            this.txtMarking.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "marking", true));
            this.txtMarking.Location = new System.Drawing.Point(588, 347);
            this.txtMarking.Name = "txtMarking";
            this.txtMarking.Size = new System.Drawing.Size(30, 20);
            this.txtMarking.TabIndex = 233;
            // 
            // txtShotPower
            // 
            this.txtShotPower.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "shot_power", true));
            this.txtShotPower.Location = new System.Drawing.Point(646, 168);
            this.txtShotPower.Name = "txtShotPower";
            this.txtShotPower.Size = new System.Drawing.Size(30, 20);
            this.txtShotPower.TabIndex = 232;
            // 
            // lblBallControl
            // 
            this.lblBallControl.AutoSize = true;
            this.lblBallControl.Location = new System.Drawing.Point(470, 291);
            this.lblBallControl.Name = "lblBallControl";
            this.lblBallControl.Size = new System.Drawing.Size(60, 13);
            this.lblBallControl.TabIndex = 231;
            this.lblBallControl.Text = "Ball Control";
            // 
            // lblCurve
            // 
            this.lblCurve.AutoSize = true;
            this.lblCurve.Location = new System.Drawing.Point(807, 232);
            this.lblCurve.Name = "lblCurve";
            this.lblCurve.Size = new System.Drawing.Size(35, 13);
            this.lblCurve.TabIndex = 230;
            this.lblCurve.Text = "Curve";
            // 
            // txtCurve
            // 
            this.txtCurve.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "curve", true));
            this.txtCurve.Location = new System.Drawing.Point(848, 229);
            this.txtCurve.Name = "txtCurve";
            this.txtCurve.Size = new System.Drawing.Size(30, 20);
            this.txtCurve.TabIndex = 229;
            // 
            // txtBallControl
            // 
            this.txtBallControl.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "ball_control", true));
            this.txtBallControl.Location = new System.Drawing.Point(537, 288);
            this.txtBallControl.Name = "txtBallControl";
            this.txtBallControl.Size = new System.Drawing.Size(30, 20);
            this.txtBallControl.TabIndex = 228;
            // 
            // lblLongPassing
            // 
            this.lblLongPassing.AutoSize = true;
            this.lblLongPassing.Location = new System.Drawing.Point(502, 232);
            this.lblLongPassing.Name = "lblLongPassing";
            this.lblLongPassing.Size = new System.Drawing.Size(71, 13);
            this.lblLongPassing.TabIndex = 227;
            this.lblLongPassing.Text = "Long Passing";
            // 
            // lblVision
            // 
            this.lblVision.AutoSize = true;
            this.lblVision.Location = new System.Drawing.Point(719, 232);
            this.lblVision.Name = "lblVision";
            this.lblVision.Size = new System.Drawing.Size(35, 13);
            this.lblVision.TabIndex = 226;
            this.lblVision.Text = "Vision";
            // 
            // txtVision
            // 
            this.txtVision.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "vision", true));
            this.txtVision.Location = new System.Drawing.Point(760, 229);
            this.txtVision.Name = "txtVision";
            this.txtVision.Size = new System.Drawing.Size(30, 20);
            this.txtVision.TabIndex = 225;
            // 
            // txtLongPassing
            // 
            this.txtLongPassing.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "long_passing", true));
            this.txtLongPassing.Location = new System.Drawing.Point(580, 229);
            this.txtLongPassing.Name = "txtLongPassing";
            this.txtLongPassing.Size = new System.Drawing.Size(30, 20);
            this.txtLongPassing.TabIndex = 224;
            // 
            // lblInterceptions
            // 
            this.lblInterceptions.AutoSize = true;
            this.lblInterceptions.Location = new System.Drawing.Point(418, 351);
            this.lblInterceptions.Name = "lblInterceptions";
            this.lblInterceptions.Size = new System.Drawing.Size(68, 13);
            this.lblInterceptions.TabIndex = 223;
            this.lblInterceptions.Text = "Interceptions";
            // 
            // lblCrossing
            // 
            this.lblCrossing.AutoSize = true;
            this.lblCrossing.Location = new System.Drawing.Point(623, 232);
            this.lblCrossing.Name = "lblCrossing";
            this.lblCrossing.Size = new System.Drawing.Size(47, 13);
            this.lblCrossing.TabIndex = 222;
            this.lblCrossing.Text = "Crossing";
            // 
            // txtCrossing
            // 
            this.txtCrossing.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "crossing", true));
            this.txtCrossing.Location = new System.Drawing.Point(675, 229);
            this.txtCrossing.Name = "txtCrossing";
            this.txtCrossing.Size = new System.Drawing.Size(30, 20);
            this.txtCrossing.TabIndex = 221;
            // 
            // txtInterceptions
            // 
            this.txtInterceptions.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "interceptions", true));
            this.txtInterceptions.Location = new System.Drawing.Point(492, 348);
            this.txtInterceptions.Name = "txtInterceptions";
            this.txtInterceptions.Size = new System.Drawing.Size(30, 20);
            this.txtInterceptions.TabIndex = 220;
            // 
            // lblDribbling
            // 
            this.lblDribbling.AutoSize = true;
            this.lblDribbling.Location = new System.Drawing.Point(371, 291);
            this.lblDribbling.Name = "lblDribbling";
            this.lblDribbling.Size = new System.Drawing.Size(48, 13);
            this.lblDribbling.TabIndex = 219;
            this.lblDribbling.Text = "Dribbling";
            // 
            // lblTackle
            // 
            this.lblTackle.AutoSize = true;
            this.lblTackle.Location = new System.Drawing.Point(756, 350);
            this.lblTackle.Name = "lblTackle";
            this.lblTackle.Size = new System.Drawing.Size(85, 13);
            this.lblTackle.TabIndex = 218;
            this.lblTackle.Text = "Standing Tackle";
            // 
            // txtDribbling
            // 
            this.txtDribbling.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "dribbling", true));
            this.txtDribbling.Location = new System.Drawing.Point(423, 288);
            this.txtDribbling.Name = "txtDribbling";
            this.txtDribbling.Size = new System.Drawing.Size(30, 20);
            this.txtDribbling.TabIndex = 217;
            // 
            // txtStandingTackle
            // 
            this.txtStandingTackle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "standing_tackle", true));
            this.txtStandingTackle.Location = new System.Drawing.Point(847, 347);
            this.txtStandingTackle.Name = "txtStandingTackle";
            this.txtStandingTackle.Size = new System.Drawing.Size(30, 20);
            this.txtStandingTackle.TabIndex = 216;
            // 
            // lblVolleys
            // 
            this.lblVolleys.AutoSize = true;
            this.lblVolleys.Location = new System.Drawing.Point(798, 170);
            this.lblVolleys.Name = "lblVolleys";
            this.lblVolleys.Size = new System.Drawing.Size(40, 13);
            this.lblVolleys.TabIndex = 215;
            this.lblVolleys.Text = "Volleys";
            // 
            // lblFinishing
            // 
            this.lblFinishing.AutoSize = true;
            this.lblFinishing.Location = new System.Drawing.Point(480, 170);
            this.lblFinishing.Name = "lblFinishing";
            this.lblFinishing.Size = new System.Drawing.Size(48, 13);
            this.lblFinishing.TabIndex = 214;
            this.lblFinishing.Text = "Finishing";
            // 
            // txtVolleys
            // 
            this.txtVolleys.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "volleys", true));
            this.txtVolleys.Location = new System.Drawing.Point(848, 167);
            this.txtVolleys.Name = "txtVolleys";
            this.txtVolleys.Size = new System.Drawing.Size(30, 20);
            this.txtVolleys.TabIndex = 213;
            // 
            // txtFinishing
            // 
            this.txtFinishing.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "finishing", true));
            this.txtFinishing.Location = new System.Drawing.Point(534, 167);
            this.txtFinishing.Name = "txtFinishing";
            this.txtFinishing.Size = new System.Drawing.Size(30, 20);
            this.txtFinishing.TabIndex = 212;
            // 
            // lblStrength
            // 
            this.lblStrength.AutoSize = true;
            this.lblStrength.Location = new System.Drawing.Point(695, 405);
            this.lblStrength.Name = "lblStrength";
            this.lblStrength.Size = new System.Drawing.Size(47, 13);
            this.lblStrength.TabIndex = 211;
            this.lblStrength.Text = "Strength";
            // 
            // lblAcceleration
            // 
            this.lblAcceleration.AutoSize = true;
            this.lblAcceleration.Location = new System.Drawing.Point(776, 113);
            this.lblAcceleration.Name = "lblAcceleration";
            this.lblAcceleration.Size = new System.Drawing.Size(66, 13);
            this.lblAcceleration.TabIndex = 210;
            this.lblAcceleration.Text = "Acceleration";
            // 
            // lblJumping
            // 
            this.lblJumping.AutoSize = true;
            this.lblJumping.Location = new System.Drawing.Point(795, 404);
            this.lblJumping.Name = "lblJumping";
            this.lblJumping.Size = new System.Drawing.Size(46, 13);
            this.lblJumping.TabIndex = 209;
            this.lblJumping.Text = "Jumping";
            // 
            // lblShortPassing
            // 
            this.lblShortPassing.AutoSize = true;
            this.lblShortPassing.Location = new System.Drawing.Point(382, 232);
            this.lblShortPassing.Name = "lblShortPassing";
            this.lblShortPassing.Size = new System.Drawing.Size(72, 13);
            this.lblShortPassing.TabIndex = 208;
            this.lblShortPassing.Text = "Short Passing";
            // 
            // lblAgility
            // 
            this.lblAgility.AutoSize = true;
            this.lblAgility.Location = new System.Drawing.Point(588, 291);
            this.lblAgility.Name = "lblAgility";
            this.lblAgility.Size = new System.Drawing.Size(34, 13);
            this.lblAgility.TabIndex = 207;
            this.lblAgility.Text = "Agility";
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Location = new System.Drawing.Point(690, 113);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(38, 13);
            this.lblSpeed.TabIndex = 206;
            this.lblSpeed.Text = "Speed";
            // 
            // txtStrength
            // 
            this.txtStrength.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "strength", true));
            this.txtStrength.Location = new System.Drawing.Point(747, 402);
            this.txtStrength.Name = "txtStrength";
            this.txtStrength.Size = new System.Drawing.Size(30, 20);
            this.txtStrength.TabIndex = 205;
            // 
            // txtJumping
            // 
            this.txtJumping.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "jumping", true));
            this.txtJumping.Location = new System.Drawing.Point(847, 401);
            this.txtJumping.Name = "txtJumping";
            this.txtJumping.Size = new System.Drawing.Size(30, 20);
            this.txtJumping.TabIndex = 204;
            // 
            // txtAgility
            // 
            this.txtAgility.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "agility", true));
            this.txtAgility.Location = new System.Drawing.Point(632, 288);
            this.txtAgility.Name = "txtAgility";
            this.txtAgility.Size = new System.Drawing.Size(30, 20);
            this.txtAgility.TabIndex = 203;
            // 
            // txtShortPassing
            // 
            this.txtShortPassing.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "short_passing", true));
            this.txtShortPassing.Location = new System.Drawing.Point(461, 229);
            this.txtShortPassing.Name = "txtShortPassing";
            this.txtShortPassing.Size = new System.Drawing.Size(30, 20);
            this.txtShortPassing.TabIndex = 202;
            // 
            // txtAcceleration
            // 
            this.txtAcceleration.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "acceleration", true));
            this.txtAcceleration.Location = new System.Drawing.Point(848, 110);
            this.txtAcceleration.Name = "txtAcceleration";
            this.txtAcceleration.Size = new System.Drawing.Size(30, 20);
            this.txtAcceleration.TabIndex = 201;
            // 
            // txtSpeed
            // 
            this.txtSpeed.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersPredictingDataBindingSource, "sprint_speed", true));
            this.txtSpeed.Location = new System.Drawing.Point(734, 110);
            this.txtSpeed.Name = "txtSpeed";
            this.txtSpeed.Size = new System.Drawing.Size(30, 20);
            this.txtSpeed.TabIndex = 200;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuStrip});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(906, 24);
            this.menuStrip1.TabIndex = 241;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileMenuStrip
            // 
            this.fileMenuStrip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createAFormationToolStripMenuItem,
            this.insertPlayerStatisticsToolStripMenuItem,
            this.playerPredictionSurveyToolStripMenuItem});
            this.fileMenuStrip.Name = "fileMenuStrip";
            this.fileMenuStrip.Size = new System.Drawing.Size(37, 20);
            this.fileMenuStrip.Text = "File";
            // 
            // createAFormationToolStripMenuItem
            // 
            this.createAFormationToolStripMenuItem.Name = "createAFormationToolStripMenuItem";
            this.createAFormationToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.createAFormationToolStripMenuItem.Text = "Create a Formation";
            // 
            // insertPlayerStatisticsToolStripMenuItem
            // 
            this.insertPlayerStatisticsToolStripMenuItem.Name = "insertPlayerStatisticsToolStripMenuItem";
            this.insertPlayerStatisticsToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.insertPlayerStatisticsToolStripMenuItem.Text = "Insert Player Statistics";
            // 
            // playerPredictionSurveyToolStripMenuItem
            // 
            this.playerPredictionSurveyToolStripMenuItem.Name = "playerPredictionSurveyToolStripMenuItem";
            this.playerPredictionSurveyToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.playerPredictionSurveyToolStripMenuItem.Text = "Player Prediction - Survey";
            // 
            // playersDataSet
            // 
            this.playersDataSet.DataSetName = "PlayersDataSet";
            this.playersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // playersPredictingDataBindingSource
            // 
            this.playersPredictingDataBindingSource.DataMember = "playersPredictingData";
            this.playersPredictingDataBindingSource.DataSource = this.playersDataSet;
            // 
            // playersPredictingDataTableAdapter
            // 
            this.playersPredictingDataTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 578);
            this.Controls.Add(this.lblScores);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblPlayerStats);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblChooseAlgorithm);
            this.Controls.Add(this.comboBoxAlgorithm);
            this.Controls.Add(this.lblPlayerPosition);
            this.Controls.Add(this.btnGeneratePosition);
            this.Controls.Add(this.txtChoosePlayer);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lblGoalkeeping);
            this.Controls.Add(this.lblReflexes);
            this.Controls.Add(this.lblHandling);
            this.Controls.Add(this.lblDiving);
            this.Controls.Add(this.lblPhysical);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDribblingText);
            this.Controls.Add(this.lblPassing);
            this.Controls.Add(this.lblShooting);
            this.Controls.Add(this.lblPace);
            this.Controls.Add(this.lblAggression);
            this.Controls.Add(this.lblSlidingTackle);
            this.Controls.Add(this.txtSlidingTackle);
            this.Controls.Add(this.txtAggression);
            this.Controls.Add(this.lblStamina);
            this.Controls.Add(this.lblLongShots);
            this.Controls.Add(this.txtLongShots);
            this.Controls.Add(this.txtStamina);
            this.Controls.Add(this.lblReactions);
            this.Controls.Add(this.lblBalance);
            this.Controls.Add(this.txtBalance);
            this.Controls.Add(this.txtReactions);
            this.Controls.Add(this.GK3);
            this.Controls.Add(this.GK2);
            this.Controls.Add(this.GK1);
            this.Controls.Add(this.txtPreferredFoot);
            this.Controls.Add(this.lblPreferredFoot);
            this.Controls.Add(this.lblShotPower);
            this.Controls.Add(this.lblMarking);
            this.Controls.Add(this.txtMarking);
            this.Controls.Add(this.txtShotPower);
            this.Controls.Add(this.lblBallControl);
            this.Controls.Add(this.lblCurve);
            this.Controls.Add(this.txtCurve);
            this.Controls.Add(this.txtBallControl);
            this.Controls.Add(this.lblLongPassing);
            this.Controls.Add(this.lblVision);
            this.Controls.Add(this.txtVision);
            this.Controls.Add(this.txtLongPassing);
            this.Controls.Add(this.lblInterceptions);
            this.Controls.Add(this.lblCrossing);
            this.Controls.Add(this.txtCrossing);
            this.Controls.Add(this.txtInterceptions);
            this.Controls.Add(this.lblDribbling);
            this.Controls.Add(this.lblTackle);
            this.Controls.Add(this.txtDribbling);
            this.Controls.Add(this.txtStandingTackle);
            this.Controls.Add(this.lblVolleys);
            this.Controls.Add(this.lblFinishing);
            this.Controls.Add(this.txtVolleys);
            this.Controls.Add(this.txtFinishing);
            this.Controls.Add(this.lblStrength);
            this.Controls.Add(this.lblAcceleration);
            this.Controls.Add(this.lblJumping);
            this.Controls.Add(this.lblShortPassing);
            this.Controls.Add(this.lblAgility);
            this.Controls.Add(this.lblSpeed);
            this.Controls.Add(this.txtStrength);
            this.Controls.Add(this.txtJumping);
            this.Controls.Add(this.txtAgility);
            this.Controls.Add(this.txtShortPassing);
            this.Controls.Add(this.txtAcceleration);
            this.Controls.Add(this.txtSpeed);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Form1";
            this.Text = "Player Position Prediction using ML";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playersDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playersPredictingDataBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblScores;
        private System.Windows.Forms.Label lblDataset;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPlayerStats;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblChooseAlgorithm;
        private System.Windows.Forms.ComboBox comboBoxAlgorithm;
        private System.Windows.Forms.Label lblPlayerPosition;
        private System.Windows.Forms.Button btnGeneratePosition;
        private System.Windows.Forms.Label txtChoosePlayer;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lblGoalkeeping;
        private System.Windows.Forms.Label lblReflexes;
        private System.Windows.Forms.Label lblHandling;
        private System.Windows.Forms.Label lblDiving;
        private System.Windows.Forms.Label lblPhysical;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDribblingText;
        private System.Windows.Forms.Label lblPassing;
        private System.Windows.Forms.Label lblShooting;
        private System.Windows.Forms.Label lblPace;
        private System.Windows.Forms.Label lblAggression;
        private System.Windows.Forms.Label lblSlidingTackle;
        private System.Windows.Forms.TextBox txtSlidingTackle;
        private System.Windows.Forms.TextBox txtAggression;
        private System.Windows.Forms.Label lblStamina;
        private System.Windows.Forms.Label lblLongShots;
        private System.Windows.Forms.TextBox txtLongShots;
        private System.Windows.Forms.TextBox txtStamina;
        private System.Windows.Forms.Label lblReactions;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.TextBox txtReactions;
        private System.Windows.Forms.TextBox GK3;
        private System.Windows.Forms.TextBox GK2;
        private System.Windows.Forms.TextBox GK1;
        private System.Windows.Forms.TextBox txtPreferredFoot;
        private System.Windows.Forms.Label lblPreferredFoot;
        private System.Windows.Forms.Label lblShotPower;
        private System.Windows.Forms.Label lblMarking;
        private System.Windows.Forms.TextBox txtMarking;
        private System.Windows.Forms.TextBox txtShotPower;
        private System.Windows.Forms.Label lblBallControl;
        private System.Windows.Forms.Label lblCurve;
        private System.Windows.Forms.TextBox txtCurve;
        private System.Windows.Forms.TextBox txtBallControl;
        private System.Windows.Forms.Label lblLongPassing;
        private System.Windows.Forms.Label lblVision;
        private System.Windows.Forms.TextBox txtVision;
        private System.Windows.Forms.TextBox txtLongPassing;
        private System.Windows.Forms.Label lblInterceptions;
        private System.Windows.Forms.Label lblCrossing;
        private System.Windows.Forms.TextBox txtCrossing;
        private System.Windows.Forms.TextBox txtInterceptions;
        private System.Windows.Forms.Label lblDribbling;
        private System.Windows.Forms.Label lblTackle;
        private System.Windows.Forms.TextBox txtDribbling;
        private System.Windows.Forms.TextBox txtStandingTackle;
        private System.Windows.Forms.Label lblVolleys;
        private System.Windows.Forms.Label lblFinishing;
        private System.Windows.Forms.TextBox txtVolleys;
        private System.Windows.Forms.TextBox txtFinishing;
        private System.Windows.Forms.Label lblStrength;
        private System.Windows.Forms.Label lblAcceleration;
        private System.Windows.Forms.Label lblJumping;
        private System.Windows.Forms.Label lblShortPassing;
        private System.Windows.Forms.Label lblAgility;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.TextBox txtStrength;
        private System.Windows.Forms.TextBox txtJumping;
        private System.Windows.Forms.TextBox txtAgility;
        private System.Windows.Forms.TextBox txtShortPassing;
        private System.Windows.Forms.TextBox txtAcceleration;
        private System.Windows.Forms.TextBox txtSpeed;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem createAFormationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertPlayerStatisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playerPredictionSurveyToolStripMenuItem;
        private PlayersDataSet playersDataSet;
        private System.Windows.Forms.BindingSource playersPredictingDataBindingSource;
        private PlayersDataSetTableAdapters.playersPredictingDataTableAdapter playersPredictingDataTableAdapter;
    }
}

