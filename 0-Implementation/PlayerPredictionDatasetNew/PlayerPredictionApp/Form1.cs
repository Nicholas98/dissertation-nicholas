﻿using PlayerPredictionDatasetNewML.Model;
using PlayerPredictionDatasetNewSVMML.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlayerPredictionApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'playersDataSet.playersPredictingData' table. You can move, or remove it, as needed.
            this.playersPredictingDataTableAdapter.Fill(this.playersDataSet.playersPredictingData);

        }

        private void lblDataset_Click(object sender, EventArgs e)
        {

        }

        private void btnGeneratePosition_Click(object sender, EventArgs e)
        {
            if (comboBoxAlgorithm.Text == "Fast Tree")
            {
                var input = new ModelInput();
                {
                    input.Name = comboBox1.Text;
                    input.Preferred_foot = txtPreferredFoot.Text;
                    input.Crossing = Convert.ToInt32(txtCrossing.Text);
                    input.Finishing = Convert.ToInt32(txtFinishing.Text);
                    input.Short_passing = Convert.ToInt32(txtShortPassing.Text);
                    input.Volleys = Convert.ToInt32(txtVolleys.Text);
                    input.Dribbling = Convert.ToInt32(txtDribbling.Text);
                    input.Curve = Convert.ToInt32(txtCurve.Text);
                    input.Long_passing = Convert.ToInt32(txtLongPassing.Text);
                    input.Ball_control = Convert.ToInt32(txtBallControl.Text);
                    input.Acceleration = Convert.ToInt32(txtAcceleration.Text);
                    input.Sprint_speed = Convert.ToInt32(txtSpeed.Text);
                    input.Agility = Convert.ToInt32(txtAgility.Text);
                    input.Reactions = Convert.ToInt32(txtReactions.Text);
                    input.Balance = Convert.ToInt32(txtBalance.Text);
                    input.Shot_power = Convert.ToInt32(txtShotPower.Text);
                    input.Jumping = Convert.ToInt32(txtJumping.Text);
                    input.Stamina = Convert.ToInt32(txtStamina.Text);
                    input.Strength = Convert.ToInt32(txtStrength.Text);
                    input.Long_shots = Convert.ToInt32(txtLongShots.Text);
                    input.Aggression = Convert.ToInt32(txtAggression.Text);
                    input.Interceptions = Convert.ToInt32(txtInterceptions.Text);
                    input.Vision = Convert.ToInt32(txtVision.Text);
                    input.Marking = Convert.ToInt32(txtMarking.Text);
                    input.Standing_tackle = Convert.ToInt32(txtStandingTackle.Text);
                    input.Sliding_tackle = Convert.ToInt32(txtSlidingTackle.Text);
                    input.Goalkeeping_diving = Convert.ToInt32(GK1.Text);
                    input.Goalkeeping_handling = Convert.ToInt32(GK2.Text);
                    input.Goalkeeping_reflexes = Convert.ToInt32(GK3.Text);
                }

                // Load model and predict output of sample data
                ModelOutput result = ConsumeModel.Predict(input);

                lblPlayerPosition.Text = result.Prediction;
                lblScores.Text = String.Join(" | ", result.Score);
                Console.WriteLine(input.Name + result.Prediction);
            }
            else if (comboBoxAlgorithm.Text == "Linear SVM")
            {
                var input = new ModelInput2();
                {
                    input.Name = comboBox1.Text;
                    input.Preferred_foot = txtPreferredFoot.Text;
                    input.Crossing = Convert.ToInt32(txtCrossing.Text);
                    input.Finishing = Convert.ToInt32(txtFinishing.Text);
                    input.Short_passing = Convert.ToInt32(txtShortPassing.Text);
                    input.Volleys = Convert.ToInt32(txtVolleys.Text);
                    input.Dribbling = Convert.ToInt32(txtDribbling.Text);
                    input.Curve = Convert.ToInt32(txtCurve.Text);
                    input.Long_passing = Convert.ToInt32(txtLongPassing.Text);
                    input.Ball_control = Convert.ToInt32(txtBallControl.Text);
                    input.Acceleration = Convert.ToInt32(txtAcceleration.Text);
                    input.Sprint_speed = Convert.ToInt32(txtSpeed.Text);
                    input.Agility = Convert.ToInt32(txtAgility.Text);
                    input.Reactions = Convert.ToInt32(txtReactions.Text);
                    input.Balance = Convert.ToInt32(txtBalance.Text);
                    input.Shot_power = Convert.ToInt32(txtShotPower.Text);
                    input.Jumping = Convert.ToInt32(txtJumping.Text);
                    input.Stamina = Convert.ToInt32(txtStamina.Text);
                    input.Strength = Convert.ToInt32(txtStrength.Text);
                    input.Long_shots = Convert.ToInt32(txtLongShots.Text);
                    input.Aggression = Convert.ToInt32(txtAggression.Text);
                    input.Interceptions = Convert.ToInt32(txtInterceptions.Text);
                    input.Vision = Convert.ToInt32(txtVision.Text);
                    input.Marking = Convert.ToInt32(txtMarking.Text);
                    input.Standing_tackle = Convert.ToInt32(txtStandingTackle.Text);
                    input.Sliding_tackle = Convert.ToInt32(txtSlidingTackle.Text);
                    input.Goalkeeping_diving = Convert.ToInt32(GK1.Text);
                    input.Goalkeeping_handling = Convert.ToInt32(GK2.Text);
                    input.Goalkeeping_reflexes = Convert.ToInt32(GK3.Text);
                }

                // Load model and predict output of sample data
                ModelOutput2 result = ConsumeModel2.Predict(input);

                lblPlayerPosition.Text = result.Prediction;
                lblScores.Text = String.Join(" | ", result.Score);
                Console.WriteLine(input.Name + result.Prediction);
            }
        }
    }
}
